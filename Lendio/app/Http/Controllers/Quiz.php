<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Quiz extends Controller
{
    // function index() {
    //     return ['name'=>'Steven Dawson'];
    // }

    function index() {
        return view('compare', ['name'=>'Steven Dawson']);
    }

    function show($id) {
        echo "Hello from controller ".$id;
    }

    function gradeQuiz(Request $req) {

        $req->validate([
            'answer'=>'required|in:Y'
        ],
        [
            'required'=>'You must answer this incredibly important question',
        ]
    );



        return view('celebrate', ['message'=>'You are Right!']);
    }
}
