<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome', function() {
    return view('welcome');
});






Route::get('quiz', 'Quiz@index');
Route::get('quiz/{id}', 'Quiz@show');

Route::view('celebrate', 'celebrate', ['message'=>'Go Take the Test']);

Route::view('quiz', 'form');
Route::post('quizresults', 'Quiz@gradeQuiz');
